class Category < ApplicationRecord

    # Gem Friendly Id
    include FriendlyId
    friendly_id :description, use: :slugged
            
    #Associações
    has_many :ads

    #Validação
    validates_presence_of :description

    #Scopes
    scope :order_by_description, -> {order(:description)}

end
